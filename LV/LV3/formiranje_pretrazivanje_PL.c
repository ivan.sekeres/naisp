#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct Node
{
    int data;
    struct Node* next;
};

typedef struct Node Node;

Node* create_linked_list(Node* head_node, int N);
int search_linked_list(Node* head_node, int number);
void free_linked_list(Node* head_node);

int main(void)
{
    int values_of_N[] = {10000, 100000, 500000, 1000000, 5000000, 10000000};

    for(int i = 0; i < sizeof(values_of_N) / sizeof(values_of_N[0]); i++)
    {
        int N = values_of_N[i];
        int number_to_search = N/2;
        int found;

        time_t creating_start_time, creating_end_time;
        time_t searching_start_time, searching_end_time;

        creating_start_time = clock();

        Node* head_node = NULL;
        Node* temp_node = NULL;

        head_node = create_linked_list(head_node, N);

        creating_end_time = clock();

        printf("Number of elements: %d\n", N);
        printf("Create time: %dms\n", (creating_end_time - creating_start_time) * 1000 / CLOCKS_PER_SEC);


        searching_start_time = clock();

        found = search_linked_list(head_node, number_to_search);

        searching_end_time = clock();

        printf("Number %d found: %d\n", number_to_search, found);
        printf("Search time: %dms\n\n", (searching_end_time - searching_start_time) * 1000 / CLOCKS_PER_SEC);

        free_linked_list(head_node);
    }

    return 0;
}

Node* create_linked_list(Node* head_node, int N)
{
    Node* last_node = NULL;
    Node* head = NULL;

    for(int i = 0; i < N; i++)
    {
        Node* new_node = (Node*)malloc(sizeof(Node));
        if(new_node == NULL)
        {
            return 0;
        }
        new_node->data = i;
        new_node->next = NULL;

        if(head_node == NULL)
        {
            head_node = new_node;
            last_node = head_node;
            head = head_node;
        }
        else
        {
            last_node->next = new_node;
            last_node = new_node;
        }
    }

    return head;
}

int search_linked_list(Node* head_node, int number)
{
    Node* temp_node = head_node;
    while(temp_node != NULL)
    {
        if(temp_node->data == number)
        {
            return 1;
        }

        temp_node = temp_node->next;
    }

    return 0;
}

void free_linked_list(Node* head_node)
{
    Node* temp_node = head_node;
    Node* current_node = head_node;
    while(temp_node != NULL)
    {
        temp_node = temp_node->next;
        free(current_node);
        current_node = temp_node;
    }
}
