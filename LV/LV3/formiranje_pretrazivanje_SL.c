#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Define the maximum level of the skip list
#define MAX_LEVEL 8

// Define the node structure for the skip list
typedef struct Node {
    int key;
    struct Node *forward[MAX_LEVEL];
} Node;

// Define the skip list structure
typedef struct SkipList {
    int level;
    struct Node *header;
} SkipList;

// Function to generate a random level for a new node
int randomLevel() {
    int level = 1;
    while (rand() < RAND_MAX / 2 && level < MAX_LEVEL) {
        level++;
    }
    return level;
}

// Function to create a new node with a given key and level
Node *createNode(int key, int level) {
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->key = key;
    for (int i = 0; i < level; i++) {
        newNode->forward[i] = NULL;
    }
    return newNode;
}

// Function to initialize a skip list
SkipList *initializeSkipList() {
    SkipList *skipList = (SkipList *)malloc(sizeof(SkipList));
    skipList->level = 1;
    skipList->header = createNode(-1, MAX_LEVEL);
    return skipList;
}

// Function to insert a key into the skip list
void insert(SkipList *skipList, int key) {
    Node *update[MAX_LEVEL];
    Node *current = skipList->header;

    for (int i = skipList->level - 1; i >= 0; i--) {
        while (current->forward[i] != NULL && current->forward[i]->key < key) {
            current = current->forward[i];
        }
        update[i] = current;
    }

    int level = randomLevel();
    if (level > skipList->level) {
        for (int i = skipList->level; i < level; i++) {
            update[i] = skipList->header;
        }
        skipList->level = level;
    }

    Node *newNode = createNode(key, level);

    for (int i = 0; i < level; i++) {
        newNode->forward[i] = update[i]->forward[i];
        update[i]->forward[i] = newNode;
    }
}

// Function to search for a key in the skip list
Node *search(SkipList *skipList, int key) {
    Node *current = skipList->header;

    for (int i = skipList->level - 1; i >= 0; i--) {
        while (current->forward[i] != NULL && current->forward[i]->key < key) {
            current = current->forward[i];
        }
    }

    // Move to the next level if the current node has a key less than or equal to the search key
    if (current->forward[0] != NULL && current->forward[0]->key == key) {
        return current->forward[0];
    }

    return NULL; // Key not found
}

double measureTimeForInsertion(SkipList *skipList, int N) {
    clock_t start_time_insert = clock();  // Start measuring time for inserting N elements

    // Insert N elements into the skip list
    for (int i = 0; i < N; i++) {
        // Use consecutive integers as keys for simplicity
        insert(skipList, i);
    }

    clock_t end_time_insert = clock();  // Stop measuring time for inserting N elements

    return ((double)(end_time_insert - start_time_insert)) * 1000 / CLOCKS_PER_SEC;  // Convert to milliseconds
}

// Function to measure the time it takes to search for a key in the skip list
double measureTimeForSearch(SkipList *skipList, int searchKey) {
    clock_t start_time_search = clock();  // Start measuring time for searching a key

    // Search for the key in the skip list
    Node *result = search(skipList, searchKey);

    clock_t end_time_search = clock();  // Stop measuring time for searching a key

    double elapsed_time_search = ((double)(end_time_search - start_time_search)) * 1000 / CLOCKS_PER_SEC;  // Convert to milliseconds

    if (result != NULL) {
        printf("Key %d found in the skip list\n", searchKey);
    } else {
        printf("Key %d not found in the skip list\n", searchKey);
    }

    return elapsed_time_search;
}

int main() {
    int N;  // Number of elements in the skip list
    printf("Enter the number of elements (N): ");
    scanf("%d", &N);

    SkipList *skipList = initializeSkipList();

    // Measure the time it takes to create the skip list
    double elapsed_time_insert = measureTimeForInsertion(skipList, N);
    printf("Time taken to create skip list with %d elements: %.3f milliseconds\n", N, elapsed_time_insert);

    int searchKey = N / 2;

    // Measure the time it takes to search for a key
    double elapsed_time_search = measureTimeForSearch(skipList, searchKey);
    printf("Time taken to search for a key in the skip list: %.3f milliseconds\n", elapsed_time_search);

    return 0;
}
