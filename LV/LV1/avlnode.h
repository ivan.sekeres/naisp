
struct AVLNode_
{
    int x;
    int height;
    struct AVLNode_ *left;
    struct AVLNode_ *right;
};
typedef struct AVLNode_ AVLNode;
