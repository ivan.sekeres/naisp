#include<stdio.h>
#include<stdlib.h>
#include <string.h>

#define MAX 10000

char * arr[MAX];

unsigned int collisions = 0;

//inicijalizacija polja na NULL
void inicijalizacija();
//hash funkcija
int hash(char * key){
    int sum=0;
    for(int i=0;i<strlen(key);i++){
        sum+=*(key+i);
    }
	return (sum % MAX);
}

void insert(char * value){
    int index = hash(value);
    int tmp;

    if(arr[index] == NULL || arr[index]==""){
        arr[index] = value;
    }else{
        tmp = index;
        while(arr[tmp]!=NULL && arr[tmp]!=""){
            if(strcmp(arr[tmp],value)==0){
                collisions++;
                printf("Već postoji!\n");
                return;
            }
            tmp++;
        }
        if(tmp==MAX){
            tmp=0;
            while(arr[tmp]!=NULL && strcmp(arr[tmp],"")!=0 && tmp<index){
                if(arr[tmp]==value){
                    return;
                }
                tmp++;
            }
            if(tmp==index){
                printf("Nema slobodnog mjesta!\n");
            }else{
                arr[tmp]=value;
            }
        }else{
            arr[tmp]=value;
        }
    }
}

int main(){
    FILE* in_file = fopen("words.txt", "r");
    char line[MAX];

    if (! in_file ){
        printf("Nije moguce procitati datoteku!\n");
        exit(-1);
    }

    for(int i = 0; i < MAX; i++)
    {
        fgets(line, MAX, in_file);
        printf(line);
        printf("\n");
        insert(line);
    }

    printf("\nbroj kolizija: %d", collisions);

    return 0;
}
