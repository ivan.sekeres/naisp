#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10000

unsigned int collisions = 0;

struct element{
    char * data;
    struct element *next;
};

struct element *ht[MAX];

//inicijalizacija polja na NULL
void inicijalizacija();
//hash funkcija
int hash(char * key){
    int sum=0;
    for(int i=0;i<strlen(key);i++){
        sum+=*(key+i);
    }
	return (sum % MAX);
}

void insert(char * value){
    struct element *novi = malloc(sizeof(struct element));
    novi->data = value;
    novi->next = NULL;

    int index = hash(value);
    if(ht[index] == NULL){
        ht[index] = novi;
    }else{
        struct element *tmp = ht[index];
        int f=0;
        while(tmp){
            if(strcmp(tmp->data, value)==0){
                f=1;
                break;
            }
            tmp = tmp->next;
        }
        if(f==0){
            novi->next = ht[index];
            ht[index]=novi;
        }else{
            collisions++;
            printf("Element vec postoji!\n");
        }
    }
}

int main(){

    FILE* in_file = fopen("words.txt", "r");
    char line[MAX];

    if (! in_file ){
        printf("Nije moguce procitati datoteku!\n");
        exit(-1);
    }

    for(int i = 0; i < MAX; i++)
    {
        fgets(line, MAX, in_file);
        printf(line);
        printf("\n");
        insert(line);
    }

    printf("\nbroj kolizija: %d", collisions);

    return 0;
}
