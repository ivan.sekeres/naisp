#include<stdio.h>
#include<stdlib.h>
#include <string.h>

#define MAX 100

char * arr[MAX];

unsigned int collisions = 0;

//inicijalizacija polja na NULL
void inicijalizacija();

//hash funkcija
int hash(char * key){
    int sum=0;
    for(int i=0;i<strlen(key);i++){
        sum+=*(key+i);
    }
	return (sum % MAX);
}

void insert(char * value){
    int index = hash(value);
    int flag=0;

    int tmp;

    if(arr[index] == NULL){
        arr[index] = value;
    }else{
        int i=0;
        while(i<MAX){
            tmp = (index+i*i)%MAX;
            if(arr[tmp]==NULL || strcmp(arr[tmp],"")==0){
                arr[tmp]=value;
                flag=1;
                break;
            }else{
                i++;
            }
        }
        if(!flag){
            collisions++;
            printf("Nema slobodnog mjesta! za %s\n",value);
        }
    }
}

int main(){
    FILE* in_file = fopen("words.txt", "r");
    char line[MAX];

    if (! in_file ){
        printf("Nije moguce procitati datoteku!\n");
        exit(-1);
    }

    for(int i = 0; i < MAX+1; i++)
    {
        fgets(line, MAX, in_file);
        printf(line);
        printf("\n");
        insert(line);
    }

    printf("\nbroj kolizija: %d", collisions);

    return 0;
}
