#include<stdio.h>
#include<stdlib.h>
#include <string.h>

#define MAX 10000
#define R 7

char * arr[MAX];

unsigned int collisions = 0;

//inicijalizacija polja na NULL
void inicijalizacija();
//hash funkcija
int hash1(char * key){
    int sum=0;
    for(int i=0;i<strlen(key);i++){
        sum+=*(key+i);
    }
	return (sum % MAX);
}
//druga hash funkcija
int hash2(char * key){
    int sum=0;
    for(int i=0;i<strlen(key);i++){
        sum+=*(key+i);
    }
    return (R - (sum % R));
}

void insert(char * value){
    int flag=0;
    int index = hash1(value);
    if(arr[index]==NULL || strcmp(arr[index],"")==0){
        arr[index] = value;
    }else{
        int i=1;
        int index2 = hash2(value);
        int tmpIndex;
        while(i<MAX){
            tmpIndex = (index + i * index2) % MAX;
            if (arr[tmpIndex] == NULL || strcmp(arr[tmpIndex],"")==0){
                arr[tmpIndex] = value;
                flag=1;
                break;
            }
            i++;
        }
        if(!flag){
            collisions++;
            printf("Nema slobodnog mjesta! za %s\n",value);
        }
    }
}

int main(){
    FILE* in_file = fopen("words.txt", "r");
    char line[MAX];

    if (! in_file ){
        printf("Nije moguce procitati datoteku!\n");
        exit(-1);
    }

    for(int i = 0; i < MAX; i++)
    {
        fgets(line, MAX, in_file);
        printf(line);
        printf("\n");
        insert(line);
    }

    printf("\nbroj kolizija: %d", collisions);

    return 0;
}
