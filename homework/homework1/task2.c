#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    srand(time(NULL));
    int n, i, j, k;
    int* matrix;
    int sum_above = 0;
    int sum_below = 0;

    printf("Enter matrix (NxN) dimension: ");
    scanf("%d", &n);

    matrix = (int *)malloc(n * n * sizeof(int));

    //matrix NxN with random numbers from 0 to 100
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            *(matrix + i * n + j) = rand() % 101;
        }
    }

    //sum of elements above secondary diagonal
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n - i; j++)
        {
            sum_above += *(matrix + i * n + j);
        }
    }
    printf("\nsum above = %d", sum_above);

    //sum of elements above secondary diagonal
    for(i = 1; i < n; i++)
    {
        for(j = n - 1; j > n - i - 1; j--)
        {
            sum_below += *(matrix + i * n + j);
        }
    }
    printf("\nsum below = %d", sum_below);


    if(sum_above > sum_below)
        printf("\nsum above > sum_below");
    else if(sum_above = sum_below)
        printf("\nsum above = sum below");
    else
        printf("\nsum above < sum below");

    free(matrix);

    return 0;
}
