#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    srand(time(NULL));
    int rows, cols, i, j;
    int** matrix;

    printf("Enter number of rows and cols of matrix separated by space (NxM): ");
    scanf("%d %d", &rows, &cols);

    matrix = (int**) malloc(rows * sizeof(int*));
    if (matrix == NULL) {
        printf("Memory allocation failed!");
        return -1;
    }

    //matrix NxN with random numbers from 0 to 100
    for(i = 0; i < rows; i++)
    {
        matrix[i] = (int*) malloc(cols * sizeof(int));

        for(j = 0; j < cols; j++)
        {
            matrix[i][j] = rand() % 101;

            if(matrix[i][j] == 100)
                printf("%d ", matrix[i][j]);
            else if(matrix[i][j] < 10)
                printf("  %d ", matrix[i][j]);
            else
                printf("% d ", matrix[i][j]);
        }

        printf("\n");
    }


    for(i = 0; i < rows; i++)
        free(matrix[i]);

    free(matrix);

    return 0;
}
