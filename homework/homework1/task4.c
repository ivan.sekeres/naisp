#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    srand(time(NULL));
    int n, i, j;
    int* matrix;


    printf("Enter matrix (NxN) dimension: ");
    scanf("%d", &n);

    matrix = (int *)malloc(n * n * sizeof(int));

    //matrix NxN with random numbers from 0 to 100
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            *(matrix + i * n + j) = rand() % 101;

            if(*(matrix + i * n + j) == 100)
                printf("%d ", *(matrix + i * n + j));
            else if(*(matrix + i * n + j) < 10)
                printf("  %d ", *(matrix + i * n + j));
            else
                printf("% d ", *(matrix + i * n + j));
        }

        printf("\n");
    }

    printf("\nElements of second diagonal:");
    j = 0;

    for(i = n - 1; i >= 0; i--)
    {
        printf("\n%d ", *(matrix + i * n + j));
        j++;
    }

    free(matrix);

    return 0;
}
