#include <stdio.h>
#include <string.h>

int main(void)
{
    char v1[20];
    char v2[20];
    int i, j, flag = 0;
    int v1_length;
    int v2_length;

    printf("Enter string V2: ");
    scanf("%s", &v2);
    printf("\nEnter string V1: ");
    scanf("%s", &v1);

    v1_length = strlen(v1);
    v2_length = strlen(v2);

    if(v1_length > v2_length)
    {
        printf("V1 is longer than V2");
        return 0;
    }

    for(i = 0; i < v2_length; i++)
    {
        if(v1[0] == v2[i])
        {
            flag = 1;
            for(j = 1; j < v1_length; j++)
            {
                if(j == v1_length - 1)
                {
                    flag = 1;
                }
                else if(v1[j] != v2[i+j])
                {
                    flag = 0;
                    break;
                }
            }
        }
    }

    if(flag)
    {
        printf("V1 is subset of V2");
    }
    else
    {
        printf("V1 is not subset of V2");
    }

    return 0;
}
